#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n,t,k,sum = 0;
    printf("Enter the number(At least of two digit):-\n");
    scanf("%d",&n);
    t = n;
    while(n != 0)
    {
        k = n % 10;
        sum = sum * 10 + k;
        n = n/10;
    }
    if(t == sum)
    {
        printf("The entered number is a palindrome");
    }
    else
    {
        printf("The entered number is not a palindrome");
    }
    return 0;
}
//write your code here