#include <stdio.h>

int main()
{
    int a,b,t;
    int *ptr,*k;
    printf("Enter the numbers you want to swap:-\n");
    scanf("%d%d",&a,&b);
    printf("before swapping a = %d b = %d\n",a,b);
    ptr = &a;
    k = &b;
    t = *ptr;
    *ptr = *k;
    *k = t;
    printf("after swapping a= %d b = %d",a,b);
    
    

    return 0;
}
