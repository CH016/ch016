#include<stdio.h>
#include<math.h>
int main()
{
  float a,b,c,D,root1,root2;
  printf("Enter the coefficients of the quadratic equation you want to check:-\n");
  scanf("%f%f%f",&a,&b,&c);
  D = b*b - 4*a*c;
  if(D >= 0)
  {
    root1 = (-b + sqrt(D))/2*a;
	root2 = (-b - sqrt(D))/2*a;
	printf("The root of the equation are %f and %f", root1,root2);
  }
  else
  {
    printf("The root of the equation are imaginary");
  }
  return 0;
}

//write your code here